package file;
///**
// * 
// */
//import java.io.File;
//import java.io.IOException;
//import org.json.JSONObject;
//public class Newfile {
//public static void main(String []args) {
//	
//	File f = new File("C:\\Users\\pkumar\\eclipse-workspace\\File_io\\src\\file\\praveen.json");//for path
//	//File f = new File("LC.text");//for creating new file
//	
//	try {
//	
//	 if (f.createNewFile()) {
//	        System.out.println("File created: " + f.getName());
//	      } else {
//	        System.out.println("File already exists.");
//	      }
//	    } catch (IOException e) {
//	      System.out.println("An error occurred.");
//	      e.printStackTrace();
//	    }
//	   
//	  
//	  JSONObject obj=new JSONObject();    
//	  obj.put("name","praveen");    
//	  obj.put("age","22");
//	  obj.put("gender","male");
//	  obj.put("profile","bda");    
//	  System.out.print(obj); 
//	   
//
//	}   
//
//}

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class Newfile {
	public static void main(String[] args) {
		
		JSONObject obj = new JSONObject();
		obj.put("name", "praveen");
		obj.put("Profile", "bda");
		obj.put("location", "Hyderabad");
		obj.put("Primary Hobbie", "Cricket");
		obj.put("age", 22);
		
		
		
		List<String> list =new ArrayList<String>();
		list.add("Coding");
		list.add("Playing");
		list.add("Riding");
		
		JSONArray array =new JSONArray();
		for(int i=0;i<list.size();i++) {
			array.put(list.get(i));
		}
		obj.put("Hobbies", array);
		
		
		try(FileWriter file =new FileWriter("C:\\Users\\pkumar\\eclipse-workspace\\File_io\\src\\file\\praveen.json")){
			file.write(obj.toString());
			file.flush();
			
		}catch(IOException e) {
			e.printStackTrace();
		}
		System.out.println(obj);
		
		
	      
	   }
	}
